#include <atltypes.h>

#include <iostream>
#include <tchar.h>


void draw_container_name(HDC dc, CRect l_name, CRect d_selection)
{
	DrawText(dc, _T("APHU312546"), 8, l_name, 0);
	DPtoLP(dc, (LPPOINT)&d_selection, 2);
	FrameRect(dc, d_selection, (HBRUSH)GetStockObject(BLACK_BRUSH));
}

int main()
{
	HDC dc = GetDC(nullptr);
	int mm = SetMapMode(dc, MM_ANISOTROPIC);
	SIZE dummy;
	SetWindowExtEx(dc, 100, 100, &dummy);
	SetViewportExtEx(dc, 10, 10, &dummy);

	CRect name{ 300, 300, 600, 400 }, selection{ 30, 30, 50, 40 };
	draw_container_name(dc, selection, name);
	draw_container_name(dc, name, selection);

	SetMapMode(dc, mm);
	ReleaseDC(nullptr, dc);
}
