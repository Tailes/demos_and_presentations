#include <atltypes.h>
#include "framework.h"

void draw_container_name(HDC dc, CRect l_name, CRect d_selection)
{
	HBRUSH br = (HBRUSH)SelectObject(dc, GetStockObject(WHITE_BRUSH));
	DPtoLP(dc, (LPPOINT)&d_selection, 2);
	POINT corners[] =
	{
		{d_selection.left, d_selection.top },
		{d_selection.right, d_selection.top },
		{d_selection.right, d_selection.bottom },
		{d_selection.left, d_selection.bottom },
		{d_selection.left, d_selection.top }
	};
	FillRect(dc, d_selection, br);
	DrawText(dc, _T("APHU312546"), 8, l_name, 0);
	Polyline(dc, corners, 5);
	SelectObject(dc, br);
}


int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow)
{
	HDC dc = GetDC(nullptr);
	int mm = SetMapMode(dc, MM_ANISOTROPIC);
	SIZE dummy;
	SetWindowExtEx(dc, 100, 100, &dummy);
	SetViewportExtEx(dc, 10, 10, &dummy);

	CRect d_bounds{ 100, 30, 220, 50 };
	CRect l_bounds{ 1000, 300, 2200, 500 };
	draw_container_name(dc, d_bounds, l_bounds);


	SetMapMode(dc, mm);
	ReleaseDC(nullptr, dc);
}

