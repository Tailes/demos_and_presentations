#include <atltypes.h>
#include <utility>
#include "framework.h"

struct Device;
struct Logical;

template<class T, class Tag>
struct Tagged : T
{
	template<class ...Us>
	explicit Tagged(Us&&... args) :
		T{ std::forward<Us>(args)... }
	{
	}
};

using CDeviceRect = Tagged<CRect, Device>;
using CLogicalRect = Tagged<CRect, Logical>;


void draw_container_name(HDC dc, CLogicalRect l_name, CDeviceRect d_selection)
{
	DrawText(dc, _T("APHU312546"), 8, l_name, 0);
	DPtoLP(dc, (LPPOINT)&d_selection, 2);
	POINT corners[] =
	{
		{d_selection.left, d_selection.top },
		{d_selection.right, d_selection.top },
		{d_selection.right, d_selection.bottom },
		{d_selection.left, d_selection.bottom },
		{d_selection.left, d_selection.top }
	};
	Polyline(dc, corners, 5);
}


int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow)
{
	HDC dc = GetDC(nullptr);
	int mm = SetMapMode(dc, MM_ANISOTROPIC);
	SIZE dummy;
	SetWindowExtEx(dc, 100, 100, &dummy);
	SetViewportExtEx(dc, 10, 10, &dummy);

	CDeviceRect d_bounds{ 100, 30, 220, 50 };
	CLogicalRect l_bounds{ 1000, 300, 2200, 500 };
	draw_container_name(dc, l_bounds, d_bounds);

	SetMapMode(dc, mm);
	ReleaseDC(nullptr, dc);
}
