#include <atltypes.h>

#include "framework.h"
#include "demo_render.hpp"


void draw_container_name(HDC dc, CRect l_name, CRect d_background)
{
	DPtoLP(dc, (LPPOINT)&d_background, 2);
	CPoint corners[] =
	{
		{d_background.left, d_background.top },
		{d_background.right, d_background.top },
		{d_background.right, d_background.bottom },
		{d_background.left, d_background.bottom },
		{d_background.left, d_background.top }
	};
	DrawText(dc, _T("APHU312546"), 8, l_name, 0);
	Polyline(dc, corners, 5);
}


void render(HDC dc)
{
	int mm = SetMapMode(dc, MM_ANISOTROPIC);
	CSize dummy;
	SetViewportExtEx(dc, 1, 1, &dummy);
	SetWindowExtEx(dc, 5, 5, &dummy);

	CRect d_bounds{ 20, 10, 90, 30 };
	CRect l_bounds{ 100, 50, 450, 150 };
	//draw_container_name(dc, d_bounds, l_bounds);
	draw_container_name(dc, l_bounds, d_bounds);

	SetMapMode(dc, mm);
}