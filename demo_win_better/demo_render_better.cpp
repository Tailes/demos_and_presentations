#include <atltypes.h>
#include <utility>
#include "framework.h"

struct Device;
struct Logical;

template<class T, class Tag>
struct Tagged : T
{
	template<class ...Us>
	explicit Tagged(Us&&... args):
		T{ std::forward<Us>(args)... }
	{
	}
};

using CDeviceRect = Tagged<CRect, Device>;
using CLogicalRect = Tagged<CRect, Logical>;


void draw_container_name(HDC dc, CLogicalRect l_name, CDeviceRect d_background)
{
	DPtoLP(dc, (LPPOINT)&d_background, 2);
	CPoint corners[] =
	{
		{d_background.left, d_background.top },
		{d_background.right, d_background.top },
		{d_background.right, d_background.bottom },
		{d_background.left, d_background.bottom },
		{d_background.left, d_background.top }
	};
	DrawText(dc, _T("APHU312546"), 8, l_name, 0);
	Polyline(dc, corners, 5);
}

void render_better(HDC dc)
{
	int mm = SetMapMode(dc, MM_ANISOTROPIC);
	CSize dummy;
	SetViewportExtEx(dc, 1, 1, &dummy);
	SetWindowExtEx(dc, 5, 5, &dummy);

	CDeviceRect d_bounds{ 20, 10, 90, 30 };
	CLogicalRect l_bounds{ 100, 50, 450, 150 };
	draw_container_name(dc, l_bounds, d_bounds);
	//draw_container_name(dc, d_bounds, l_bounds);
	//draw_container_name(dc, CLogicalRect{ d_bounds }, CDeviceRect{ l_bounds });

	SetMapMode(dc, mm);
}
