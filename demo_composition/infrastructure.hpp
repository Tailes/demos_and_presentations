#pragma once
#include <type_traits>


namespace composition
{
	template<class T>
	class state {};

	namespace details
	{
		template<class T, class S>
		using is_state_of = std::is_base_of<state<T>, S>;

		template<class Fn, class S, class = std::void_t<>>
		struct is_callable : std::false_type {};
		template<class Fn, class S>
		struct is_callable<Fn, S, std::void_t<std::result_of_t<Fn(S&&)>>> : std::true_type {};

		template<class T, class S>
		using is_not_state_of = std::negation<is_state_of<T, S>>;
		template<class T, class Fn, class S>
		using is_applicable_state = std::conjunction<is_state_of<T, S>, is_callable<Fn, S>>;
		template<class T, class Fn, class S>
		using is_inapplicable_state = std::conjunction<is_state_of<T, S>, std::negation<is_callable<Fn, S>>>;
	}
	struct ignored {};

	struct composition
	//class composition
	{
		
	//protected:
		template<class T, class Fn, class S>
		static constexpr auto call_getter(Fn&& getter, S&& arg)->std::enable_if_t<details::is_not_state_of<T, S>::value, S&&>;
		template<class T, class Fn, class S>
		static constexpr auto call_getter(Fn&& getter, S&& arg)->std::enable_if_t<details::is_inapplicable_state<T, Fn, S>::value, ignored>;
		template<class T, class Fn, class S>
		static constexpr auto call_getter(Fn&& getter, S&& arg)->std::enable_if_t<details::is_applicable_state<T, Fn, S>::value, std::result_of_t<Fn(S&&)>>;
		
		template<class ...Done>
		struct invoker
		{
			template<class Target, class Call>
			static constexpr auto invoke(Target&& trg, Call&& call, Done&&... done);
			template<class Target, class Call, class U, class ...Rest>
			static constexpr auto invoke(Target&& trg, Call&& call, Done&&... done, U&& arg, Rest&&... rest);
			template<class Target, class Call, class ...Rest>
			static constexpr auto invoke(Target&& trg, Call&& call, Done&&... done, ignored&&, Rest&&... rest);
		};
	};

	template<class T, class Fn, class S>
	inline constexpr auto composition::call_getter(Fn&& getter, S&& arg) -> std::enable_if_t<details::is_not_state_of<T, S>::value, S&&>
	{
		return std::forward<S>(arg);
	}

	template<class T, class Fn, class S>
	inline constexpr auto composition::call_getter(Fn&& getter, S&& arg) -> std::enable_if_t<details::is_inapplicable_state<T, Fn, S>::value, ignored>
	{
		return {};
	}

	template<class T, class Fn, class S>
	inline constexpr auto composition::call_getter(Fn&& getter, S&& arg) -> std::enable_if_t<details::is_applicable_state<T, Fn, S>::value, std::result_of_t<Fn(S&&)>>
	{
		return std::forward<Fn>(getter)(std::forward<S>(arg));
	}

	template<class ...Done>
	template<class Target, class Call>
	inline constexpr auto composition::invoker<Done...>::invoke(Target&& trg, Call&& call, Done&& ...done)
	{
		return std::forward<Call>(call)(std::forward<Target>(trg), std::forward<Done>(done)...);
	}

	template<class ...Done>
	template<class Target, class Call, class U, class ...Rest>
	inline constexpr auto composition::invoker<Done...>::invoke(Target&& trg, Call&& call, Done&& ...done, U&& arg, Rest&& ...rest)
	{
		using nxt = invoker<Done..., U>;
		return nxt::invoke(std::forward<Target>(trg), std::forward<Call>(call), std::forward<Done>(done)..., std::forward<U>(arg), std::forward<Rest>(rest)...);
	}

	template<class ...Done>
	template<class Target, class Call, class ...Rest>
	inline constexpr auto composition::invoker<Done...>::invoke(Target&& trg, Call&& call, Done&& ...done, ignored&&, Rest&& ...rest)
	{
		using nxt = invoker<Done...>;
		return nxt::invoke(std::forward<Target>(trg), std::forward<Call>(call), std::forward<Done>(done)..., std::forward<Rest>(rest)...);
	}


	template<class Impl>
	class compound :
		public composition
	{
		template<class ...Args>
		static constexpr decltype(auto) assembler(Args&&...args);
	protected:
		template<class Target, class Call, class Fn, class ...Ts>
		static constexpr decltype(auto) call_child(Target&& target, Call&& call, Fn&& getter, Ts&& ...args);
		template<class Target, class Call, class Fn, class ...Ts>
		static constexpr decltype(auto) call_child_namesake(Target&& target, Call&& call, Fn&& getter, Ts&& ...args);
		template<class ...Subjects,class Target, class Call, class Fn, class ...Ts>
		static constexpr decltype(auto) call_children_by_type(Target&& target, Call&& call, Fn&& getter, Ts&& ...args);
	};

	template<class Impl>
	template<class ...Args>
	inline constexpr decltype(auto) compound<Impl>::assembler(Args&& ...args)
	{
	}

	template<class Impl>
	template<class Target, class Call, class Fn, class ...Ts>
	inline constexpr decltype(auto) compound<Impl>::call_child(Target&& target, Call&& call, Fn&& getter, Ts&& ...args)
	{
		return invoker<>::invoke(std::forward<Target>(target), std::forward<Call>(call), call_getter<Impl>(std::forward<Fn>(getter), std::forward<Ts>(args))...);
	}

	template<class Impl>
	template<class Target, class Call, class Fn, class ...Ts>
	inline constexpr decltype(auto) compound<Impl>::call_child_namesake(Target&& target, Call&& call, Fn&& getter, Ts&& ...args)
	{
		return invoker<>::invoke(std::forward<Fn>(getter)(std::forward<Target>(target)), std::forward<Call>(call), call_getter<Impl>(std::forward<Fn>(getter), std::forward<Ts>(args))...);
	}

}


template<class T, class Tag>
class tagged :
	public T
{
public:
	template<class... Args>
	explicit tagged(Args&&... args) :
		T{ std::forward<Args>(args)... }
	{
	}
};