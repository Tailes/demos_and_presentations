#pragma once

#include "infrastructure.hpp"
#include "surface.hpp"
#include <iostream>


using composition::state;

//  +--------------------------------------------------------+
//  |                         Track                          |
//  |                                                        |
//  | +----------------------------------------------------+ |
//  | | Railcar                                            | |
//  | |                                                    | |
//  | |      +#####################################+       | |
//  | |      #                                     #       | |
//  | |      #             Container               #       | |
//  | |      #                                     #       | |
//  | |      +#####################################+       | |
//  | |                                                    | |
//  | | +------------------------------------------------+ | |
//  | | |                     Frame                      | | |
//  | | +------------------------------------------------+ | |
//  | |                                                    | |
//  | +----------------------------------------------------+ |
//  |                                                        |
//  +--------------------------------------------------------+


class container;




class cn_selected :
	public state<container>
{
public:
	int get_selection_param() const { return 123; }
};




class cn_highlighted :
	public state<container>
{
public:
	bool get_highlight_param() const { return false; }
};



class cn_locked :
	public state<container>
{
public:
	double get_lock_param() const { return 3.1415; }
};




class container
{
public:
	void draw(surface& surf) const;
	void draw(surface& surf, const cn_locked& st) const;
	void draw(surface& surf, const cn_selected& st) const;
	void draw(surface& surf, const cn_highlighted& st) const;
	void draw(surface& surf, const cn_selected& selection, const cn_highlighted& highlight) const;
};

void container::draw(surface& surf) const
{
	std::cout << "[default]\n";
}

void container::draw(surface& surf, const cn_locked& st) const
{
	std::cout << "[[[locked, " << st.get_lock_param() << "]]]\n";
}

void container::draw(surface& surf, const cn_selected& st) const
{
	std::cout << "[selected, " << st.get_selection_param() << "]\n";
}

void container::draw(surface& surf, const cn_highlighted& st) const
{
	std::cout << "[highlighted, " << st.get_highlight_param() << "]\n";
}

void container::draw(surface& surf, const cn_selected& selection, const cn_highlighted& highlight) const
{
	std::cout << "[selected, " << selection.get_selection_param() << "; highlighted, " << highlight.get_highlight_param() << "]\n";
}
