#pragma once

class drawable
{
public:

protected:
	auto static make_draw_call();
};



auto drawable::make_draw_call()
{
	return [](auto&& trg, auto&&... args)
	{
		return std::forward<decltype(trg)>(trg).draw(std::forward<decltype(args)>(args)...);
	};
}