#pragma once
#include "infrastructure.hpp"
#include "railcar.hpp"
#include "surface.hpp"
#include "support.hpp"

#include <iostream>
#include <algorithm>
#include <iterator>


// +###############################################################################################################+
// #                                                     View                                                      #
// #                                                                                                               #
// # +----------------------------------------------------+ +----------------------------------------------------+ #
// # | First Railcar                                      | | Last Railcar                                       | #
// # |                                                    | |                                                    | #
// # |      +-------------------------------------+       | |      +-------------------------------------+       | #
// # |      |                                     |       | |      |                                     |       | #
// # |      |            Mid Container            |       | |      |            Mid Container            |       | #
// # |      |                                     |       | |      |                                     |       | #
// # |      +-------------------------------------+       | |      +-------------------------------------+       | #
// # |                                                    | |                                                    | #
// # |      +-----------------+ +-----------------+       | |      +-----------------+ +-----------------+       | #
// # |      |                 | |                 |       | |      |                 | |                 |       | #
// # |      |  Left Container | | Right Container |       | |      |  Left Container | | Right Container |       | #
// # |      |                 | |                 |       | |      |                 | |                 |       | #
// # |      +-----------------+ +-----------------+       | |      +-----------------+ +-----------------+       | #
// # |                                                    | |                                                    | #
// # | +------------------------------------------------+ | | +------------------------------------------------+ | #
// # | |                     Frame                      | | | |                     Frame                      | | #
// # | +------------------------------------------------+ | | +------------------------------------------------+ | #
// # |                                                    | |                                                    | #
// # +----------------------------------------------------+ +----------------------------------------------------+ #
// #                                                                                                               #
// +###############################################################################################################+




using composition::compound;



class view;

struct last;
struct first;

using last_railcar = tagged<railcar, last>;
using first_railcar = tagged<railcar, first>;


class tr_selected :
	public state<view>
{
public:
	rc_selected get_last_railcar() const { return {}; };
};




class tr_highlighted :
	public state<view>
{
public:
	rc_highlighted get_last_railcar() const { return {}; };
};




class tr_locked :
	public state<view>
{
public:
	rc_locked get_first_railcar() const { return {}; };
};




class view :
	public drawable,
	public compound<view>
{
public:
	template<typename ...Args>
	void draw(surface & surf, Args&&... args)const;

private:
	const auto& get_first_railcar() const { return m_first_railcar; }
	const auto& get_last_railcar() const { return m_last_railcar; }

	auto& get_first_railcar() { return m_first_railcar; }
	auto& get_last_railcar() { return m_last_railcar; }

	// member access templates =============================
	template<class T>
	static auto make_get();
	template<> static auto make_get<first_railcar>();
	template<> static auto make_get<last_railcar>();

private:
	first_railcar m_first_railcar;
	last_railcar m_last_railcar;
};

template<> static auto view::make_get<first_railcar>()
{
	return [](auto&& trg) -> decltype(std::declval<decltype(trg)>().get_first_railcar())
	{
		return std::forward<decltype(trg)>(trg).get_first_railcar();
	};
}

template<> static auto view::make_get<last_railcar>()
{
	return [](auto&& trg) -> decltype(std::declval<decltype(trg)>().get_last_railcar())
	{
		return std::forward<decltype(trg)>(trg).get_last_railcar();
	};
}

template<typename ...Args>
void view::draw(surface& surf, Args&&... args) const
{
	call_child_namesake(*this, make_draw_call(), make_get<first_railcar>(), surf, std::forward<Args>(args)...);
	std::cout << " +\n";
	call_child_namesake(*this, make_draw_call(), make_get<last_railcar>(), surf, std::forward<Args>(args)...);
}

