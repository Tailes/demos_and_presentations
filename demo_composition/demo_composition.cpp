#include "infrastructure.hpp"
#include "surface.hpp"
#include "track.hpp"

#include <iostream>
#include <cassert>




int main()
{
	surface s;
	std::cout << "drawing default\n";
	track{}.draw(s);
	std::cout << "\n\ndrawing selected\n";
	track{}.draw(s, tr_selected{});
	std::cout << "\n\ndrawing locked\n";
	track{}.draw(s, tr_locked{});
	std::cout << "\n\ndrawing highlighted\n";
	track{}.draw(s, tr_highlighted{});
	std::cout << "\n\ndrawing selected highlighted\n";
	track{}.draw(s, tr_selected{}, tr_highlighted{});
	std::getchar();
}
