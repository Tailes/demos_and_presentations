#pragma once

#include "infrastructure.hpp"
#include "container.hpp"
#include "support.hpp"
#include "surface.hpp"
#include "frame.hpp"

#include <iostream>
#include <algorithm>
#include <iterator>

using composition::compound;



//  +--------------------------------------------------------+
//  |                         Track                          |
//  |                                                        |
//  | +####################################################+ |
//  | # Railcar                                            # |
//  | #                                                    # |
//  | #      +-------------------------------------+       # |
//  | #      |                                     |       # |
//  | #      |            Mid Container            |       # |
//  | #      |                                     |       # |
//  | #      +-------------------------------------+       # |
//  | #                                                    # |
//  | #      +-----------------+ +-----------------+       # |
//  | #      |                 | |                 |       # |
//  | #      |  Left Container | | Right Container |       # |
//  | #      |                 | |                 |       # |
//  | #      +-----------------+ +-----------------+       # |
//  | #                                                    # |
//  | # +------------------------------------------------+ # |
//  | # |                     Frame                      | # |
//  | # +------------------------------------------------+ # |
//  | #                                                    # |
//  | +####################################################+ |
//  |                                                        |
//  +--------------------------------------------------------+




class railcar;

struct top;
struct left;
struct right;

using top_container = tagged<container, top>;
using left_container = tagged<container, left>;
using right_container = tagged<container, right>;



class rc_selected :
	public state<railcar>
{
public:
	cn_selected get_left_container() const { return {}; };
	cn_selected get_top_container() const { return {}; };
};




class rc_highlighted :
	public state<railcar>
{
public:
	cn_highlighted get_right_container() const { return {}; };
	cn_highlighted get_top_container() const { return {}; };
};



class rc_locked :
	public state<railcar>
{
public:
	cn_locked get_top_container() const { return {}; };
};




class railcar :
	public drawable,
	public compound<railcar>
{
public:
	template<typename ...Args>
	void draw(surface& surf, Args&&... args)const;

private:
	const auto& get_right_container() const { return m_right_container; }
	const auto& get_left_container() const { return m_left_container; }
	const auto& get_top_container() const { return m_top_container; }
	const auto& get_frame() const { return m_frame; }

	auto& get_right_container() { return m_right_container; }
	auto& get_left_container() { return m_left_container; }
	auto& get_top_container() { return m_top_container; }
	auto& get_frame() { return m_frame; }

	// member access templates =============================
	template<class T>
	static auto make_get();
	template<> static auto make_get<right_container>();
	template<> static auto make_get<left_container>();
	template<> static auto make_get<top_container>();
	template<> static auto make_get<frame>();
	
private:
	right_container m_right_container;
	left_container m_left_container;
	top_container m_top_container;
	frame m_frame;
};





template<> static auto railcar::make_get<right_container>()
{
	return [](auto&& trg) -> decltype(std::declval<decltype(trg)>().get_right_container())
	{
		return std::forward<decltype(trg)>(trg).get_right_container();
	};
}

template<> static auto railcar::make_get<left_container>()
{
	return [](auto&& trg) -> decltype(std::declval<decltype(trg)>().get_left_container())
	{
		return std::forward<decltype(trg)>(trg).get_left_container();
	};
}

template<> static auto railcar::make_get<top_container>()
{
	return [](auto&& trg) -> decltype(std::declval<decltype(trg)>().get_top_container())
	{
		return std::forward<decltype(trg)>(trg).get_top_container();
	};
}

template<> static auto railcar::make_get<frame>()
{
	return [](auto&& trg) -> decltype(std::declval<decltype(trg)>().get_frame())
	{
		return std::forward<decltype(trg)>(trg).get_frame();
	};
}


////call_child(m_frame, make_draw_call(), make_get_frame(), std::forward<Args>(args)...);
////call_child(m_left_container, make_draw_call(), make_get_left_container(), std::forward<Args>(args)...);
////call_child(m_top_container, make_draw_call(), make_get_top_container(), std::forward<Args>(args)...);
////call_child(m_right_container, make_draw_call(), make_get_right_container(), std::forward<Args>(args)...);
//call_child(make_get<frame>()(*this), make_draw_call(), make_get_frame(), std::forward<Args>(args)...);
//call_child(make_get<left_container>()(*this), make_draw_call(), make_get_left_container(), std::forward<Args>(args)...);
//call_child(make_get<top_container>()(*this), make_draw_call(), make_get_top_container(), std::forward<Args>(args)...);
//call_child(make_get<right_container>()(*this), make_draw_call(), make_get_right_container(), std::forward<Args>(args)...);

template<typename ...Args>
void railcar::draw(surface& surf, Args&&... args) const
{
	std::cout << " |\n";
	std::cout << "o|"; call_child_namesake(*this, make_draw_call(), make_get<frame>(), surf, std::forward<Args>(args)...);
	std::cout << " |"; call_child_namesake(*this, make_draw_call(), make_get<top_container>(), surf, std::forward<Args>(args)...);
	std::cout << " |"; call_child_namesake(*this, make_draw_call(), make_get<left_container>(),surf, std::forward<Args>(args)...);
	std::cout << "o|"; call_child_namesake(*this, make_draw_call(), make_get<right_container>(), surf, std::forward<Args>(args)...);
	std::cout << " |\n";
}


