#include <iostream>
#include <type_traits>
#include <utility>


template<class ...Ts, class T, class Fn, class ...Us>
decltype(auto) test(T&& arg, Fn&& call, Us&&... params)
{
	return std::forward<Fn>(call)(std::forward<T>(arg), std::forward<Us>(params)...);
}



static auto fn = [](auto&& target, auto&&... args)
{
	return 11;
};




int main()
{
	std::cout << "Hello World!\n";
	std::cout << test<float, double, bool>(19, fn, true, false, true);
}
